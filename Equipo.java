
//Esta es la clase objeto llamada Equipo 

public class Equipo
{
	//Declaracion de los atributos
	private String tipo;
	private int codigo;
	private String marca;
	private String estado;
	private String modelo;


	public Equipo ()
	{
		tipo= " ";
		codigo= 0;
		marca= " ";
		estado= " ";
		modelo= " ";

	}//constructor sin parametros 

	public Equipo (String tipo, int codigo, String marca, String estado, String modelo)
	{
		this.tipo=tipo;
		this.codigo=codigo;
		this.marca=marca;
		this.estado=estado;
		this.modelo=modelo;

	}//constructor con parametros, es el metodo que se va a utilizar 

	public void setTipo (String tipo)
	{
		this.tipo=tipo;
	}//fin setTipo
		
	public String getTipo ()
	{
		return tipo;
	}//fin getTipo
		
	public void setCodigo (int codigo)
	{
		this.codigo=codigo;
	}// fin setCodigo
		
	public int getCodigo ()
	{
		return codigo;
	}// fin getCodigo
	
	public  void setMarca (String marca)
	{
		this.marca=marca;
	}// fin setMarca
		
	public String getMarca ()
	{
		return marca;
	}// fin getMarca
	
	public void setEstado (String estado)
	{
		this.estado=estado;
	}// fin setEstado
		
	public String getEstado ()
	{
		return estado;
	}//fin getEstado
	
	public void setModelo (String modelo)
	{
		this.modelo= modelo;
	}//fin setModelo
		
	public String getModelo ()
	{
		return modelo;
	}//fin getModelo
	
	
	public String toString ()
	{
		return "\nEl tipo del equipo que se presto es: "+ getTipo()+
							"\nEl codigo del equipo que se presto es: "+getCodigo()+
							"\nLa marca del equipo que se presto es: "+getMarca()+
							"\nEl estado del equipo que se presto es: "+getEstado()+
							"\nEl modelo del equipo que se presto es: "+getModelo();
							
	}//fin del metodo toString, encargado de mostrar los datos
	
}//fin clase
