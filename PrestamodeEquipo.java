
//Clase principal llamada PrestamodeEquipo 

import javax.swing.JOptionPane;

public class PrestamodeEquipo
{
	public static void main (String arg [])
	{
		//Declaracion de las variables 
		String nombre, usuario, fechaPrestamo, fechaDevolucion, estado, datoLeido, listaProfesores, listaEstudiantes;
		String tipo, marca, modelo;
		int identificacion, placa, opcion;
		int codigo;
		listaProfesores="Lista de Docentes\n";
		listaEstudiantes="Lista de Estudiantes\n";
			
		//Inicializacion de las variables 
		tipo= " ";
		codigo= 0;
		marca= " ";
		estado= " ";
		modelo= " ";
			
			
		Equipo miEquipo= null; 
			
			
		JOptionPane.showMessageDialog (null, "***En este programa se llevara un registro de los equipos prestados  por la Coordinacion de carrera de ITM***");
		do
		{
			datoLeido= JOptionPane.showInputDialog ("*******Opciones*******\n"+
														"1. Ingresar datos."+
														"\n2. Mostrar datos."+
														"\n3.Salir.");
			opcion=Integer.parseInt (datoLeido);
			switch (opcion)
			{
				case 1:
	
						usuario= JOptionPane.showInputDialog ("Indique si estudiante o docente: ");
						if  (usuario.equalsIgnoreCase("estudiante"))
						{
							datoLeido= JOptionPane.showInputDialog ("Digite su identificacion: ");
							identificacion= Integer.parseInt (datoLeido);
			
							nombre= JOptionPane.showInputDialog ("Digite su nombre: ");
		
							datoLeido= JOptionPane.showInputDialog ("Digite la placa del equipo prestado: ");
							placa= Integer.parseInt (datoLeido);
			
							fechaPrestamo= JOptionPane.showInputDialog ("Digite la fecha en la que solicito el equipo: ");
			
							fechaDevolucion= JOptionPane.showInputDialog ("Digite la fecha en la que se devolvera el equipo: ");
			
							miEquipo= new Equipo ( tipo,  codigo,  marca,  estado,  modelo);
			
							tipo=JOptionPane.showInputDialog ("Ingrese el tipo del equipo: ");
							miEquipo.setTipo (tipo);
										
							datoLeido= JOptionPane.showInputDialog ("Ingrese el codigo del equipo: ");
							codigo= Integer.parseInt (datoLeido);
							miEquipo.setCodigo (codigo);
										
							marca= JOptionPane.showInputDialog ("Ingrese la marca del equipo: ");
							miEquipo.setMarca (marca);
										
							modelo= JOptionPane.showInputDialog ("Ingrese el modelo del equipo: ");
							miEquipo.setModelo (modelo);
			
							estado= JOptionPane.showInputDialog ("Digite las observaciones: ");
							miEquipo.setEstado(estado);
										
										

							listaEstudiantes= listaEstudiantes+"\nNombre: "+nombre+"\nIdentificacion: "+identificacion+"\nPlaca: "+placa+
											 "\nFecha de Prestamo: "+fechaPrestamo+"\nFecha de Devolucion: "+fechaDevolucion+ miEquipo.toString()+
										    "\n*****************\n";
						}
						else 
						{
							datoLeido= JOptionPane.showInputDialog ("Digite su identificacion: ");
							identificacion= Integer.parseInt (datoLeido);
			
						    nombre= JOptionPane.showInputDialog ("Digite su nombre: ");
		
							datoLeido= JOptionPane.showInputDialog ("Digite la placa del equipo prestado: ");
							placa= Integer.parseInt (datoLeido);
			
							fechaPrestamo= JOptionPane.showInputDialog ("Digite la fecha en la que solicito el equipo: ");
			
							fechaDevolucion= JOptionPane.showInputDialog ("Digite la fecha en la que se devolvera el equipo: ");
										
							miEquipo= new Equipo ( tipo,  codigo,  marca,  estado,  modelo);
										
							tipo=JOptionPane.showInputDialog ("Ingrese el tipo del equipo: ");
							miEquipo.setTipo (tipo);
										
							datoLeido= JOptionPane.showInputDialog ("Ingrese el codigo del equipo: ");
							codigo= Integer.parseInt (datoLeido);
							miEquipo.setCodigo (codigo);			
										
							marca= JOptionPane.showInputDialog ("Ingrese la marca del equipo: ");
							miEquipo.setMarca (marca);
										
							modelo= JOptionPane.showInputDialog ("Ingrese el modelo del equipo: ");
							miEquipo.setModelo (modelo);
			
							estado= JOptionPane.showInputDialog ("Digite las observaciones: ");
							miEquipo.setEstado(estado);
										
							listaProfesores= listaProfesores+ "\nNombre: "+nombre+"\nIdentificacion: "+identificacion+"\nPlaca: "+placa+
									  	"\nFecha de Prestamo: "+fechaPrestamo+"\nFecha de Devolucion: "+fechaDevolucion+ miEquipo.toString()+ 
									  	"\n*****************\n";
						}			
			
						break;
							
						case 2:
								if (miEquipo!=null)
								{
									JOptionPane.showMessageDialog (null, "*******Registro de los equipos prestados por estudiante*******\n"+listaEstudiantes);
									JOptionPane.showMessageDialog (null, "*******Registro de los equipos prestados por docente*******\n"+listaProfesores);
								}
								else
								{
									JOptionPane.showMessageDialog (null, "No se ha registrado ningun equipo");
								}	
						break;
							
						case 3:
								JOptionPane.showMessageDialog (null, "Fin del programa");

						break;
							
						default: JOptionPane.showMessageDialog(null, "Opcion no disponible.");
							
							
			}//fin switch
					
		}while (opcion!= 3); //fin del do-while
			

			
	}//fin del metodo main 
	
}//fin clase
